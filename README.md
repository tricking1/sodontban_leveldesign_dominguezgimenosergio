# PEC3 of Level Design: So-Don't-Ban (Sokoban), by Sergio Domínguez

## The Game
Sokoban is a classic game developed for the Spectrum computer. I have implemented a simpler version of the game, in which you can create your own levels via a integrated editor! Push the blocks to the yellow spots to complete the level.

![GAME](game.png)

## The Editor
The editir is a in-game built editor that I have used myself to develop this project. The levels are stored in the Assets/Levels folder. To use it, just drag and drop the blocks in the grid. You have to use as many white blocks as yellow blocks. You have to place mandatory the green block as well, although you want to start the level out it :)

## How to play
- __WASD__: Movement
- __Mouse__: Selecting or dragging blocks
- __Left Click__: Interact

## Explanatory videos
I have recorded two videos (in spanish) of me [playing my levels](https://youtu.be/RJMuywKu6JU) and another one of me [explaining the editor](https://youtu.be/aIQDH2m4qxs).