using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanController : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        // Debug.Log("Collision with can");

        if (collision.gameObject.GetComponent<DragNDropBlock>().isDestructible)
            Destroy(collision.gameObject);
    }
}
