using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DragNDropBlock : MonoBehaviour
{
    public DragNDropBlock droppablePrefab;

    public float rayDistance = 1.0f; // look for adjacent blocks

    public bool isDragging;
    public float minimumDroppingDistance;
    public bool isDraggable;
    public bool isSource = false; // if it is the source of the rest of blocks
    public bool isGoal = false;
    private bool isGoalAndActive = false;
    public bool isDestructible = false;
    public int numberOfGoalsTouched = 0; // avoid that one white block activates two goals at the same time

    public bool isTouchingPlayer = false;
    Vector2 playerNormal = Vector2.zero;
    public bool isTouchingBlock = false;
    Vector2 blockNormal = Vector2.zero;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // only drag and drop in editor scene
        if (SceneManager.GetActiveScene().name == "Editor")
        {
            // when mouse is pressed
            if (Input.GetMouseButtonDown(0))
            {
                var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.back);

                /* if (hit) Debug.Log("hit is true");
                else Debug.Log("hit is false"); */

                if (hit && hit.collider.gameObject == this.gameObject) // I condition it this way because all scripts are executing at the same time in the three blocks
                {
                    if (hit.collider.gameObject.GetComponent<DragNDropBlock>().isSource && droppablePrefab)
                    {
                        DragNDropBlock newBlock = Instantiate(droppablePrefab);
                        newBlock.GetComponent<DragNDropBlock>().isDestructible = true;
                        newBlock.GetComponent<DragNDropBlock>().isDragging = true;
                    }
                    else if (hit.collider.gameObject.GetComponent<DragNDropBlock>().isDraggable)
                    {
                        hit.collider.gameObject.GetComponent<DragNDropBlock>().isDragging = true;
                    }
                }
            }

            if (isDragging && isDraggable && Input.GetMouseButton(0))
            {
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePosition.z = -2.0f;
                transform.position = mousePosition;
            }

            // what to do when mouse is released
            if (Input.GetMouseButtonUp(0) && isDragging && isDraggable)
            {
                int i = 0;

                isDragging = false;

                // Let's calculate the closest distance to the available gaps: find all gaps
                GameObject[] allGaps = GameObject.FindGameObjectsWithTag("BlockGap");
                Vector3[] allGapsPositions = new Vector3[allGaps.Length];
                float minimumDistance = 1000.0f;
                int minimumIndex = 255;

                // Find the gap that is the closest to our actual position
                for (i = 0; i < allGaps.Length; i++)
                {
                    allGapsPositions[i] = allGaps[i].transform.position;
                    float distance = Vector3.Distance(allGapsPositions[i], transform.position);

                    // Debug.Log("Current distance is " + distance);

                    if (distance < minimumDistance && distance < minimumDroppingDistance)
                    {
                        minimumIndex = i;
                        minimumDistance = Vector3.Distance(allGapsPositions[i], transform.position);
                        // Debug.Log("Shortest distance is " + minimumDistance + " of object " + allGaps[i].name);
                    }
                }

                GetComponent<BoxCollider2D>().enabled = true;

                if (minimumIndex != 255) transform.position = new Vector3(allGapsPositions[minimumIndex].x, allGapsPositions[minimumIndex].y, -2.0f);
            }
        }
    }

    public bool GetIsGoalAndActive()
    {
        return isGoalAndActive;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // avoid blocks to be moved by other blocks
        if (gameObject.CompareTag("Block"))
        {
            if (collision.collider.gameObject.CompareTag("Player") && Input.anyKey)
            {
                isTouchingPlayer = true;
                playerNormal = collision.contacts[0].normal;
                Debug.Log("Normal is " + playerNormal);
                Debug.Log("Input is " + (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));

                // only move the block if the player is pressing the same direction
                if (playerNormal == new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")))
                {
                    Vector2 position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

                    // find all objects on the other side of the pushed block (they are stacked, so need to see if there is not a white block or a wall
                    bool isWall = false, isBlock = false, isGap = false;

                    Collider2D[] colliders = Physics2D.OverlapPointAll(position + playerNormal);

                    foreach (Collider2D collider in colliders)
                    {
                        if (collider.CompareTag("Wall"))
                        {
                            isWall = true;
                        }
                        else if (collider.CompareTag("Block"))
                        {
                            isBlock = true;
                        }
                        else if (collider.CompareTag("BlockGap"))
                        {
                            isGap = true;
                        }
                    }

                    if (isGap && !isWall && !isBlock)
                    {
                        transform.position = colliders[0].transform.position;
                    }
            }
            }
        }
    }

    /* Logic to check if the game is solved */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isGoal)
        {
            // if a white block is placed on the goal, set active.
            if (collision.gameObject.CompareTag("Block")) isGoalAndActive = true;
        }

        // I am going to avoid that one block can activate two positions at the same time
        if (gameObject.CompareTag("Block") && collision.gameObject.CompareTag("Goal")) numberOfGoalsTouched++;

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isGoal && isGoalAndActive)
        {
            if (collision.gameObject.CompareTag("Block")) isGoalAndActive = false;
        }

        // I am going to avoid that one block can activate two positions at the same time
        if (gameObject.CompareTag("Block") && collision.gameObject.CompareTag("Goal")) numberOfGoalsTouched--;
    }
}
