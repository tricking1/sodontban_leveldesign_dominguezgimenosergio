using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EditorManager : MonoBehaviour
{
    private FileManager m_FileManager;
    private BlockDataArray allBlocksArray; //this one is serialized
    private BlockData[] allBlocks;

    public GameObject wallPrefab;
    public GameObject blockPrefab;
    public GameObject goalPrefab;
    public GameObject startPrefab;
    public GameObject startPoint;

    private GameObject[] allBlocksGameObject;
    private GameObject[] allWalls;
    private GameObject[] allGoals;
    private GameObject[] startBlock; // only one starting position can be placed

    // public GameObject startPoint; // set to inactive if level is being edited

    public TextMeshProUGUI levelName;
    public TextMeshProUGUI placeholderString;

    // Start is called before the first frame update
    void Start()
    {
        m_FileManager = FindObjectOfType<FileManager>();
        allBlocksArray = new BlockDataArray();

        // in case that we are editing one certain level, place its blocks and keep it selected
        if (m_FileManager && m_FileManager.oneLevelIsSelected)
        {
            // Debug.Log("Placing prefabs");
            placeholderString.text = m_FileManager.selectedLevel; // write the level name in the inputField
            levelName.text = "";
            PlacePrefabs();
            startPoint.SetActive(false); // if the level is loaded, don't place the starting point and keep the previous one
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlacePrefabs()
    {
        // Load all blocks from the file
        allBlocks = m_FileManager.LoadArray(m_FileManager.selectedLevel);

        Debug.Log("Loaded " + allBlocks.Length + " blocks");

        // Instantiate prefabs where it is saved
        for (int i = 0; i < allBlocks.Length; i++)
        {
            switch (allBlocks[i].typeOfBlock)
            {
                case TypesOfBlock.Wall:
                    Instantiate(wallPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    break;

                case TypesOfBlock.Block:
                    Instantiate(blockPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    break;

                case TypesOfBlock.Goal:
                    Instantiate(goalPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    break;

                case TypesOfBlock.Start:
                    Instantiate(startPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    break;
            }

            Debug.Log("Placing block " + i + " in position " + allBlocks[i].position);
        }
    }

    public void SaveLevel()
    {
        BlockData block;

        FindAllBlocks();

        allBlocksArray.arrayOfBlocks = new BlockData[allBlocksGameObject.Length + allWalls.Length + allGoals.Length + 1];

        // block data
        for (int i = 0; i < allBlocksGameObject.Length; i++)
        {
            block = new BlockData();
            block.position = allBlocksGameObject[i].transform.position;
            block.rotation = allBlocksGameObject[i].transform.rotation;
            block.typeOfBlock = TypesOfBlock.Block;
            allBlocksArray.arrayOfBlocks[i] = block;
        }

        // wall data
        for (int i = 0; i < allWalls.Length; i++)
        {
            block = new BlockData();
            block.position = allWalls[i].transform.position;
            block.rotation = allWalls[i].transform.rotation;
            block.typeOfBlock = TypesOfBlock.Wall;
            allBlocksArray.arrayOfBlocks[i + allBlocksGameObject.Length] = block;
        }

        // goal data
        for (int i = 0; i < allGoals.Length; i++)
        {
            block = new BlockData();
            block.position = allGoals[i].transform.position;
            block.rotation = allGoals[i].transform.rotation;
            block.typeOfBlock = TypesOfBlock.Goal;
            allBlocksArray.arrayOfBlocks[i + allBlocksGameObject.Length + allWalls.Length] = block;
        }

        // start position data
        block = new BlockData();
        block.position = startBlock[0].transform.position;
        block.rotation = startBlock[0].transform.rotation;
        block.typeOfBlock = TypesOfBlock.Start;
        allBlocksArray.arrayOfBlocks[allBlocksGameObject.Length + allWalls.Length + allGoals.Length] = block;

        // If it is empty (length = 1), save with the original name
        if (levelName.text.Length == 1 && m_FileManager.oneLevelIsSelected) m_FileManager.SaveArray(allBlocksArray, m_FileManager.selectedLevel + ".json");
        else m_FileManager.SaveArray(allBlocksArray, levelName.text + ".json");

        Debug.Log("levelName is " + levelName.text.Length);
    }

    public void FindAllBlocks()
    {
        allBlocksGameObject = GameObject.FindGameObjectsWithTag("Block");
        allWalls = GameObject.FindGameObjectsWithTag("Wall");
        allGoals = GameObject.FindGameObjectsWithTag("Goal");
        startBlock = GameObject.FindGameObjectsWithTag("Start"); // only one starting position can be placed
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void DeleteAllBlocks()
    {
        FindAllBlocks();

        // block data
        for (int i = 0; i < allBlocksGameObject.Length; i++)
        {
            Destroy(allBlocksGameObject[i]);
        }

        // wall data
        for (int i = 0; i < allWalls.Length; i++)
        {
            Destroy(allWalls[i]);
        }

        // goal data
        for (int i = 0; i < allGoals.Length; i++)
        {
            Destroy(allGoals[i]);
        }
    }

    public void TryLevel()
    {
        m_FileManager.selectedLevel = levelName.text;
        SceneManager.LoadScene("Game");
    }

}
