using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;

public enum TypesOfBlock
{
    Wall,
    Block,
    Goal,
    Start
}

[Serializable]
public class BlockData
{
    public Vector3 position;
    public Quaternion rotation;
    public TypesOfBlock typeOfBlock;
}

[Serializable] // necessary to save as json
public class BlockDataArray
{
    public BlockData[] arrayOfBlocks;
}

public class FileManager : MonoBehaviour
{
    public string[] allLevelsNames;
    public string selectedLevel;
    public int numberCurrentLevel;
    public bool oneLevelIsSelected;
    public bool allLevelsRequested; // if the player wants to play all levels one by one

    private void Start()
    {
        DontDestroyOnLoad(this);
        LoadAllLevels();
    }

    public void LoadAllLevels()
    {
        allLevelsNames = Directory.GetFiles(@"Assets/Levels/", "*.json");

        // eliminate extension and directory
        if (allLevelsNames.Length > 0)
        {
            for (int i = 0; i < allLevelsNames.Length; i++)
            {
                // eliminate directory
                allLevelsNames[i] = allLevelsNames[i].Split('/')[2]; // get only the last element, which is the name of file with extension

                // eliminate extension
                allLevelsNames[i] = allLevelsNames[i].Split('.')[0]; //get only first element, which is the name of file
            }
        }
    }

    public void SaveArray(BlockDataArray blockArray, string filename)
    {
        // int[] numbers = new int[] { 1, 2, 3 };
        // string json = JsonUtility.ToJson(numbers);

        string json = JsonUtility.ToJson(blockArray);
        Debug.Log("json contains " + json);
        // filePath = Path.Combine(Application.persistentDataPath, destiny);
        File.WriteAllText(@"Assets/Levels/" + filename, json);
        Debug.Log("File saved in " + @"Assets/Levels/" + filename);

        // update levels list after saving
        LoadAllLevels();

    }

    public BlockData[] LoadArray(string filename) //filename without extension
    {
        Debug.Log("Requesting file named" + "Assets/Levels/" + filename + ".json");
        if (File.Exists(@"Assets/Levels/" + filename + ".json"))
        {
            Debug.Log("File exists named " + @"Assets/Levels/" + filename + ".json");
            string json = File.ReadAllText(@"Assets/Levels/" + filename + ".json");
            BlockDataArray myArray = JsonUtility.FromJson<BlockDataArray>(json);
            return myArray.arrayOfBlocks;
        }
        else
        {
            Debug.Log("FILE DOESN'T EXIST");
        }

        return null;

        
    }

    public void DeleteLevel(string filename)
    {
        File.Delete(@"Assets/Levels/" + filename + ".json");
        File.Delete(@"Assets/Levels/" + filename + ".json.meta");
        Debug.Log(filename + ".json file has been deleted");

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
        LoadAllLevels();
    }
}


