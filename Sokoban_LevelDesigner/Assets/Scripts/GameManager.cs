using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private PlayerController Player;

    private FileManager m_FileManager;
    private BlockData[] allBlocks;
    private AudioSource m_AudioSource;

    public AudioClip levelCompletedClip;

    private bool musicPlayed = false;
    public bool firstSpawn = true; // check if it is the first time that the blocks are spawned

    public Text levelName;

    public GameObject wallPrefab;
    public GameObject blockPrefab;
    public GameObject goalPrefab;
    public GameObject startPrefab;

    public GameObject[] allBlocksGameObject;
    public GameObject[] allWalls;
    public GameObject[] allGoals;
    public GameObject[] allGaps;

    public int goalsActive;
    public int numberOfGoals;
    public int numberOfWalls;
    public int numberOfBlocks;

    // Start is called before the first frame update
    void Start()
    {
        Player = FindObjectOfType<PlayerController>();
        Player.isControllable = true;

        m_FileManager = FindObjectOfType<FileManager>();
        m_AudioSource = GetComponent<AudioSource>();

        goalsActive = 0;

        // spawn all blocks
        Debug.Log("Placing prefabs");
        levelName.text = m_FileManager.selectedLevel;
        PlacePrefabs();

        // get first amount of blocks, because later, Unity duplicates it, don't know why
        numberOfGoals = GameObject.FindGameObjectsWithTag("Goal").Length;
        numberOfWalls = GameObject.FindGameObjectsWithTag("Wall").Length;
        numberOfBlocks = GameObject.FindGameObjectsWithTag("Block").Length;

        // dont show the gaps
        allGaps = GameObject.FindGameObjectsWithTag("BlockGap");
        for (int i = 0; i < allGaps.Length; i++)
        {
            allGaps[i].GetComponent<SpriteRenderer>().enabled = false;
            allGaps[i].AddComponent<BoxCollider2D>(); // add a box colider to be detectable
            allGaps[i].GetComponent<BoxCollider2D>().isTrigger = true;
        }

            FindAllBlocks();

        m_AudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        AllGoalsAreActive();

        if (goalsActive == allGoals.Length)
        {
            LevelIsCompleted();
        }
    }

    public void AllGoalsAreActive()
    {
        // here I need to check if any white block is touching two goals at the same time
        int num = 0;

        for (int i = 0; i < allGoals.Length; i++)
        {
            if (allGoals[i] != null && 
                allGoals[i].GetComponent<DragNDropBlock>().GetIsGoalAndActive()) num++;
        }

        for (int i = 0; i < allBlocksGameObject.Length; i++)
        {
            // if any white block is touching more than one goal, discard one
            if (allBlocksGameObject[i].GetComponent<DragNDropBlock>().numberOfGoalsTouched > 1)
            {
                num--;
            }
        }

        goalsActive = num;
        numberOfGoals = allGoals.Length;
    }

    public void PlacePrefabs()
    {
        GameObject block;

        // Load all blocks from the file
        allBlocks = m_FileManager.LoadArray(m_FileManager.selectedLevel);

        Debug.Log("Loaded " + allBlocks.Length + " blocks");

        // Instantiate prefabs where it is saved
        for (int i = 0; i < allBlocks.Length; i++)
        {
            switch (allBlocks[i].typeOfBlock)
            {
                case TypesOfBlock.Wall:
                    block = Instantiate(wallPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                    block.GetComponent<BoxCollider2D>().isTrigger = false; // goal can be gone through
                    break;

                case TypesOfBlock.Block:
                    block = Instantiate(blockPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                    block.GetComponent<BoxCollider2D>().isTrigger = false; // goal can be gone through
                    break;

                case TypesOfBlock.Goal:
                    block = Instantiate(goalPrefab, allBlocks[i].position, allBlocks[i].rotation);
                    block.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                    block.GetComponent<BoxCollider2D>().isTrigger = true; // goal can be gone through
                    break;

                case TypesOfBlock.Start:
                    Player.transform.position = allBlocks[i].position; // place player in starting position
                    break;
            }

            // Debug.Log("Placing block " + i + " in position " + allBlocks[i].position);
        }
    }

    public void LevelIsCompleted()
    {
        // Debug.Log("Level completed!!");

        StartCoroutine(LoadNextLevel());

        if(!musicPlayed)
        {
            m_AudioSource.volume = 0.1f; // this clip is very loud

            // pause and play the music
            m_AudioSource.Pause();
            m_AudioSource.Play();
            m_AudioSource.PlayOneShot(levelCompletedClip);
            musicPlayed = true; // play only once
        }
    }

    public void FindAllBlocks()
    {
        allBlocksGameObject = GameObject.FindGameObjectsWithTag("Block");
        allWalls = GameObject.FindGameObjectsWithTag("Wall");
        allGoals = GameObject.FindGameObjectsWithTag("Goal");
    }

    public void DeleteAllBlocks()
    {
        // block data
        for (int i = 0; i < allBlocksGameObject.Length; i++)
        {
            Destroy(allBlocksGameObject[i]);
        }

        // wall data
        for (int i = 0; i < allWalls.Length; i++)
        {
            Destroy(allWalls[i]);
        }

        // goal data
        for (int i = 0; i < allGoals.Length; i++)
        {
            Destroy(allGoals[i]);
        }
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene("Game");
    }

    IEnumerator LoadNextLevel()
    {
        yield return new WaitForSeconds(levelCompletedClip.length);

        if(m_FileManager.numberCurrentLevel < (m_FileManager.allLevelsNames.Length - 1) && 
            m_FileManager.allLevelsRequested) // -1 because later I will increase the index
        {
            m_FileManager.numberCurrentLevel++; // select next level
            m_FileManager.selectedLevel = m_FileManager.allLevelsNames[m_FileManager.numberCurrentLevel];
            SceneManager.LoadScene("Game");
        }
        else
        {
            // if this was the last level or not all levels are requested, go to main menu
            SceneManager.LoadScene("MainMenu");
        }
            

        
    }
}
