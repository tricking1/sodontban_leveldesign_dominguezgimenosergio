using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelManagerController : MonoBehaviour
{
    private TMP_Dropdown dropdown;
    private FileManager fileManager;

    private string defaultString = "No levels available";

    // Start is called before the first frame update
    void Start()
    {
        dropdown = FindObjectOfType<TMP_Dropdown>();
        fileManager = FindAnyObjectByType<FileManager>();
        fileManager.oneLevelIsSelected = false;
        SetDropDownOptions();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDropDownOptions()
    {
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();

        if (fileManager.allLevelsNames.Length > 0)
        {
            for (int i = 0; i < fileManager.allLevelsNames.Length; i++)
            {
                // fill dropdown menu with names from the filemanager
                options.Add(new TMP_Dropdown.OptionData(fileManager.allLevelsNames[i]));
            }

            fileManager.selectedLevel = fileManager.allLevelsNames[0]; //set first element as selected one if exists
            fileManager.oneLevelIsSelected = true;
        }
        else
        {
            options.Add(new TMP_Dropdown.OptionData(defaultString));
            
        }
        dropdown.options = options;
    }

    public void HandleInputData(int val)
    {
        fileManager.selectedLevel = dropdown.options[val].text;

        // if it is not empty
        if(dropdown.options[val].text != defaultString)
        {
            Debug.Log("Selected level is " + fileManager.selectedLevel);
            fileManager.oneLevelIsSelected = true;
        }
            

    }

    public void DeleteLevel()
    {
        fileManager.DeleteLevel(fileManager.selectedLevel);
        SetDropDownOptions();
    }

    public void GoToEditor()
    {
        SceneManager.LoadScene("Editor");
    }

    public void GoToGame()
    {
        if(fileManager.oneLevelIsSelected) SceneManager.LoadScene("Game");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
