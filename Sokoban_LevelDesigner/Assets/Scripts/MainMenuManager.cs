using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    private FileManager fileManager;

    // Start is called before the first frame update
    void Start()
    {
        fileManager = FindObjectOfType<FileManager>();

        fileManager.oneLevelIsSelected = false;
        fileManager.allLevelsRequested = false;
        fileManager.numberCurrentLevel = 0; // in case all levels are requested
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToEditor()
    {
        SceneManager.LoadScene("Editor");
    }

    public void GoToLevelManager()
    {
        SceneManager.LoadScene("LevelManager");
    }

    public void GoToAllLevels()
    {
        fileManager.allLevelsRequested = true;
        fileManager.selectedLevel = fileManager.allLevelsNames[0]; //start by first level
        SceneManager.LoadScene("Game");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
