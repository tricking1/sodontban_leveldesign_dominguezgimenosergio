using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public bool isControllable;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        isControllable = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isControllable)
        {
            ReadInput();
        }
    }

    public void ReadInput()
    {
        // Obtiene la entrada del teclado
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Calcula la direcci�n de movimiento
        Vector3 direction = new Vector3(horizontal, vertical, 0);

        // Mueve al jugador en la direcci�n calculada
        rb.AddForce(direction * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
    }
}
